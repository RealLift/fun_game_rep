float dir = 0;

void setup(){
  fullScreen();
  float x = 3.14;
  float y = 2.72;
}

void draw(){
  rect(200, 200, 300, 400);
  dir += 0.01;
  fill(0, 255, 0);
  ellipse((50*(sin(dir))+width/2-10),(50*(cos(dir))+height/2-10),20,20);
  ellipse(10, 20, 50, 40);
  ellipse(10, 20, 50, 40);
}
